#+TITLE: Review of 2-3 tree Experiment
#+AUTHOR: VLEAD
#+DATE: [2019-08-29 Thursday] 
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the review comments for the
  =2-3 tree= experiment.
  
* Review Comments
  + Please find the review comments for an experiment. Once
  the issue is fixed, update the status of an issue in the
  below table.  Also, comment and add label(eg:
  Resolved/Differed etc.,) to the Git issue to track the
  status.
  + Make sure you put descriptions of all the variables that
    are used in the Javascript. Description should say what
    the variable does and where it is used.

  |------+-----------+--------+----------------------------|
  | S.NO | Project   | Issues | Resolved(Y/N) and Comments |
  |------+-----------+--------+----------------------------|
  |   1. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/6][#6]]     |                            |
  |------+-----------+--------+----------------------------|
  |   2. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/7][#7]]     |                            |
  |------+-----------+--------+----------------------------|
  |   3. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/8][#8]]     |                            |
  |------+-----------+--------+----------------------------|
  |   4. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/9][#9]]     |                            |
  |------+-----------+--------+----------------------------|
  |   5. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/10][#10]]    |                            |
  |------+-----------+--------+----------------------------|
  |   6. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/11][#11]]    |                            |
  |------+-----------+--------+----------------------------|
  |   7. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/12][#12]]    |                            |
  |------+-----------+--------+----------------------------|
  |   8. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/13][#13]]    |                            |
  |------+-----------+--------+----------------------------|
  |   9. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/14][#14]]    |                            |
  |------+-----------+--------+----------------------------|
  |  10. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/15][#15]]    |                            |
  |------+-----------+--------+----------------------------|
  |  11. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/16][#16]]    |                            |
  |------+-----------+--------+----------------------------|
  |  12. | Artefacts | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/17][#17]]    |                            |
  |------+-----------+--------+----------------------------|
  |  13. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/2][#2]]     |                            |
  |------+-----------+--------+----------------------------|
  |  14. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/3][#3]]     |                            |
  |------+-----------+--------+----------------------------|
  |  15. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/4][#4]]     |                            |
  |------+-----------+--------+----------------------------|
  |  16. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/5][#5]]     |                            |
  |------+-----------+--------+----------------------------|
  |  17. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/6][#6]]     |                            |
  |------+-----------+--------+----------------------------|
  |  18. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/7][#7]]     |                            |
  |------+-----------+--------+----------------------------|
  |  19. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/8][#8]]     |                            |
  |------+-----------+--------+----------------------------|
  |  20. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/9][#9]]     |                            |
  |------+-----------+--------+----------------------------|
  |  21. | Content   | [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/content-html/issues/10][#10]]    |                            |
  |------+-----------+--------+----------------------------|

 
