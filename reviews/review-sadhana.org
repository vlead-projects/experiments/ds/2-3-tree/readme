#+TITLE: Review of 2-3 Tree Experiment
#+AUTHOR: VLEAD
#+DATE: [2019-05-06 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
* Review Comments
  Please find the overall review comments below:
** Documentation
   1. Readme repository doesn't have any documents related
      to experiment. The same is created as an issue
      [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/readme/issues/1][here]]. Please include all the documents and update the
      issue.

   2. Create an "Index" document listed down with all the
      artefacts. The same is created as an issue
      [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/1][here]]. Include the document and update the issue.

** Code 
   1. It seems like all the Interactive HTML artefacts in
      this experiment are taken from web.
      
      Risks using third party code:
      + Make sure it's an open source code, otherwise we
        might face copyright issues.

      + Not easy to maintain, as there is no enough
        explanation to code.

   2. Indentation of the code is not followed in many
      places. Please correct it.

   3. Literate for the code is not written
      properly. Elaborate and write the description for each
      code block.

   4. Used comments inside the code blocks to describe
      it. Please remove comments from the code block and add
      them as description to the code block.

   5. Remove commented code from the content,
      artefacts(HTML, JS), etc., for better readability.

   6. Remove inline JS from the HTML aretfacts.
   
   7. Remove inline css from HTML artefacts.
      Example:  
      
   8. The entire styles are placed in one entire code block
      in org. Styles for each DOM element must be placed as
      a single block with a proper description i.e., for
      which DOM element this style is been applied.
   
   9. There is a lot of commented code in the style
      sheets. Remove it to make the code look
      cleaner.

   10. HTML is not organized properly in org. Please refer
       [[][Bubble Sort]] for reference and make it exactly
       like that.
      
   11. Each code block in org should be given an unique
       name.

** UI and Functionality
   1. Unclear instructions and improper alignment in
      interactive artefacts.  The same is recorded as an
      issue [[https://gitlab.com/vlead-projects/experiments/ds/2-3tree/artefacts/issues/2][here]]. Please fix and update it.
      

